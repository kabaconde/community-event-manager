## Composer
FROM composer:latest as composer_build
WORKDIR /app
COPY ./composer.json /app
COPY ./composer.lock /app
COPY ./.env.dist /app/.env
RUN composer install

## WebpackEncore
FROM node:11 as node_build
WORKDIR /var/www/symfony
COPY ./webpack.config.js /var/www/symfony/webpack.config.js
COPY ./assets /var/www/symfony/assets
COPY ./package.json /var/www/symfony/package.json
COPY ./yarn.lock /var/www/symfony/yarn.lock
COPY ./.env.dist /var/www/symfony/.env
RUN mkdir -p /var/www/symfony/public/build
ENV NODE_ENV=development
RUN yarn install \
    && yarn encore dev

## Apache
FROM php:7.3-apache
COPY ./ /var/www/symfony
COPY --from=node_build /var/www/symfony/public/build /var/www/symfony/public/build
COPY ./.env.dist /var/www/symfony/.env
COPY --from=composer_build /app/vendor /var/www/symfony/vendor

RUN rm -rf config/packages/test var docker \
    && a2enmod rewrite \
    && apt-get update && apt-get install --no-install-recommends --assume-yes --quiet ca-certificates libpq-dev \
    && docker-php-ext-install pdo pdo_pgsql pgsql \
    && apt-get clean && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /var/www/symfony/var/cache/dev \
    && mkdir -p /var/www/symfony/var/log \
    && chown -R www-data:www-data /var/www/symfony/var

## Copy configuration
COPY ./docker/php-apache/virtualhost.conf /etc/apache2/sites-enabled/000-default.conf
COPY ./docker/php-apache/apache2.conf /etc/apache2/conf-enabled/apache2.conf
COPY ./docker/php-apache/php.ini /usr/local/etc/php/php.ini

WORKDIR /var/www/symfony
EXPOSE 80
