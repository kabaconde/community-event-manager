## Composer
FROM composer:latest as composer_build
WORKDIR /app
COPY ./composer.json /app
COPY ./composer.lock /app
COPY ./.env.dist /app/.env
ENV APP_ENV=prod
ENV APP_DEBUG=0
RUN composer install --no-dev --optimize-autoloader --no-scripts --ignore-platform-reqs

## WebpackEncore
FROM node:11 as node_build
WORKDIR /var/www/symfony
COPY ./webpack.config.js /var/www/symfony/webpack.config.js
COPY ./assets /var/www/symfony/assets
COPY ./package.json /var/www/symfony/package.json
COPY ./yarn.lock /var/www/symfony/yarn.lock
RUN mkdir -p /var/www/symfony/public/build
ENV NODE_ENV=development
RUN yarn install \
    && yarn encore production \
    && rm -rf node_modules

## Apache
FROM php:7.3-apache
COPY ./ /var/www/symfony
COPY --from=node_build /var/www/symfony/public/build /var/www/symfony/public/build
COPY --from=composer_build /app/vendor /var/www/symfony/vendor
ENV APP_ENV=prod
ENV APP_DEBUG=0
WORKDIR /var/www/symfony
RUN rm -rf config/packages/test var docker \
    && a2enmod rewrite \
    && apt-get update && apt-get install --no-install-recommends --assume-yes --quiet ca-certificates libpq-dev \
    && docker-php-ext-install pdo pdo_pgsql pgsql opcache \
    && docker-php-ext-enable opcache \
    && apt-get clean && rm -rf /var/lib/apt/lists/* \
    && APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear \
    && mkdir -p /var/www/symfony/var/cache/prod /var/www/symfony/var/log \
    && chown -R www-data:www-data /var/www/symfony/var

## Copy configuration
COPY ./docker/php-apache/virtualhost.conf /etc/apache2/sites-enabled/000-default.conf
COPY ./docker/php-apache/apache2.conf /etc/apache2/conf-enabled/apache2.conf
COPY ./docker/php-apache/php.ini /usr/local/etc/php/php.ini

WORKDIR /var/www/symfony
EXPOSE 80
